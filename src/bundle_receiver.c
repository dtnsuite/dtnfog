/********************************************************
    Authors:
    Lorenzo Mustich, lorenzo.mustich@studio.unibo.it
	Lorenzo Tullini, lorenzo.tullini@studio.unibo.it
    Carlo Caini (DTNfog project supervisor), carlo.caini@unibo.it

    License:
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Copyright (c) 2018, Alma Mater Studiorum, University of Bologna

 ********************************************************/

/**
 * bundle_receiver.c
 * Thread to receive file vai BP protocol,
 * it uses Semaphores (POSIX) to avoid concurrency whith tcp sender side.
 */

#include "../../unified_api/src/al/utilities/debug_print/al_utilities_debug_print.h"
#include "proxy_thread.h"
#include "bundle_header_utility.h"
#include "utility.h"

/* ---------------------------
 *      Global variables
 * --------------------------- */
static al_types_bundle_object bundle;

/* -------------------------------
 *       Function interfaces
 * ------------------------------- */
int read_dtnperf_header(char* filename);
static void criticalError(void *arg);

/**
 * Thread code
 */
void * bundleReceiving(void * arg) {
	al_error unified_api_error;
	int error;
	bp_receiver_info_t * proxy_inf = (bp_receiver_info_t *) arg;

	al_types_bundle_payload_location location = BP_PAYLOAD_FILE;
	al_types_endpoint_id source_eid;

	char * file_name_payload;
	uint file_name_payload_len;
	char filename[FILE_NAME];
	char temp_filename[256];

	int fd, fdNew;
	char char_read;

	int index = 0;

	//Changing of default exit routine
	pthread_cleanup_push(criticalError, NULL);

	opendir(DTN_DIR);
	if(errno == ENOENT) {
		al_utilities_debug_print(proxy_inf->debug_level, "[DEBUG] Creating %s\n", DTN_DIR);
		mkdir(DTN_DIR, 0700);
	}

	//Start daemon like execution
	while(1==1) {
		//Create bundle
		unified_api_error = al_bundle_create(&bundle);
		if (unified_api_error != AL_SUCCESS) {
			al_utilities_debug_print_error("Error in al_bundle_create()");
			al_bundle_free(&bundle);
			continue;
		}
		printf("Waiting for a bundle...\n");

		unified_api_error=al_socket_receive(proxy_inf->rd_receive, &bundle, location, -1);
		while (unified_api_error!= AL_SUCCESS) {
			if(unified_api_error==AL_ERROR){
				break;
			}
			printf("Registration busy\n");
			unified_api_error=al_socket_receive(proxy_inf->rd_receive, &bundle, location, -1);
		}
		if(unified_api_error== AL_ERROR){
			break;
		}
		printf("Bundle received\n");

		/*utility_error = al_socket_receive(proxy_inf->rd_receive, &bundle, location, -1);
		if (utility_error != BP_EXTB_SUCCESS) {
			al_utilities_debug_print_error("Error in al_bundle_get_file_name_payload_file() (%s)\n", al_bp_strerror(utility_error));
			al_bundle_free(&bundle);
			if(utility_error==BP_EXTB_ERRRECEIVE){
				break;
			}
			continue;
		}*/
		printf("Bundle received\n");

		unified_api_error = al_bundle_get_payload_file(bundle, &file_name_payload, &file_name_payload_len);
		if (unified_api_error != AL_SUCCESS) {
			al_bundle_free(&bundle);
			continue;
		}

		unified_api_error = al_bundle_get_source(bundle, &source_eid);
		if (unified_api_error != AL_SUCCESS) {
			al_bundle_free(&bundle);
			continue;
		}

		al_utilities_debug_print(proxy_inf->debug_level,
				"[DEBUG] file_name: %s EID_src: %s\n", basename(file_name_payload), source_eid.uri);

		circular_buffer_item toSend;
		if(proxy_inf->options == 'n') {
			fd = open(file_name_payload, O_RDONLY);
			if (fd < 0) {
				al_utilities_debug_print_error("Opening file %s failed (%s)\n", file_name_payload, strerror(errno));
				al_bundle_free(&bundle);
				continue;
			}
			strcpy(temp_filename, CLC_DIR);
			strcat(temp_filename, &source_eid.uri[4]);
			strcat(temp_filename, "_");
			strcat(temp_filename, &file_name_payload[5]);
			sprintf(filename, "%s_%d", temp_filename, index);
			strcat(filename, ".tar");

			fdNew = open(filename, O_WRONLY | O_CREAT, 0700);
			if (fd < 0) {
				al_utilities_debug_print_error("Reading file %s failed (%s)\n", file_name_payload, strerror(errno));
				al_bundle_free(&bundle);
				close(fd);
				continue;
			}
			int result;
			while(read(fd, &char_read, sizeof(char)) > 0) {
				//TODO: aggiustare con un controllo il valore di ritorno della write altrimenti compare warning
				result = write(fdNew, &char_read, sizeof(char));
				if(result < 0) {
					al_utilities_debug_print_error("Writing failed\n");
				}
			}
			close(fd);
			close(fdNew);
			strcpy(toSend.fileName, filename);
		}
		else { //DTNperf compatibility
			HEADER_TYPE bundle_header;
			if (get_dtnperf_bundle_header_and_options(&bundle, &bundle_header) < 0) {
					al_utilities_debug_print_error("Error in getting bundle header and options\n");
					al_bundle_free(&bundle);
					continue;
			}

			error = read_dtnperf_header(toSend.fileName);

			if(error != BP_SUCCESS) {
					al_utilities_debug_print_error("Error in reading dtnperf header (%s)\n", al_socket_str_type_error(error));
					al_bundle_free(&bundle);
					continue;
				}
			}
		unified_api_error = al_bundle_free(&bundle);
		if (unified_api_error != AL_SUCCESS) {
			al_bundle_free(&bundle);
			//set_is_running_to_false(proxy_inf->mutex, proxy_inf->is_running);
			//kill(proxy_inf->tid_snd, SIGUSR1);
			break;
		}
		strcpy(filename, "");
		toSend.protocol=0;
		circular_buffer_push(proxy_inf->calcol_unit_circular_buffer,toSend);


	}//while

	//Signaling to main an error in daemon like execution
	proxy_inf->error=unified_api_error;
	kill(getpid(),SIGINT);
	pthread_cleanup_pop(1);
	return NULL;
}

/**
 * Function for dtnperf compability
 */
int read_dtnperf_header(char * filename) {
	FILE *pl_stream;
	char *transfer;
	int transfer_len;
	uint32_t pl_size;

	//Get info about bundle size
	al_bundle_get_payload_size(bundle, &pl_size);

	if (dtnperf_open_payload_stream_read(bundle, &pl_stream) < 0){
		al_utilities_debug_print_error("Error in opening file transfer bundle (%s)\n", strerror(errno));
		return BP_ERRBASE;
	}

	transfer_len = HEADER_SIZE + BUNDLE_OPT_SIZE+sizeof(al_types_timeval);
	transfer = (char*) malloc(transfer_len);
	memset(transfer, 0, transfer_len);

	if (fread(transfer, transfer_len, 1, pl_stream) != 1 && ferror(pl_stream)!=0){
		al_utilities_debug_print_error("Error in processing file transfer bundle (%s)\n", strerror(errno));
		return BP_ERRBASE;
	}
	free(transfer);

	fseek(pl_stream, BUNDLE_CRC_SIZE, SEEK_CUR);

	transfer_len = pl_size-transfer_len-BUNDLE_CRC_SIZE;
	transfer = (char*) malloc(transfer_len);
	memset(transfer, 0, transfer_len);

	if (fread(transfer, transfer_len, 1, pl_stream) != 1 && ferror(pl_stream)!=0){
		al_utilities_debug_print_error("Error in processing file transfer bundle (%s)\n", strerror(errno));
		return BP_ERRBASE;
	}

	free(transfer);
	dtnperf_close_payload_stream_read(pl_stream);

	dtnperf_process_incoming_file(filename, &bundle);

	return BP_SUCCESS;
}


/**
 * Custom routine started in case of reception of pthread_cancel by parent.
 */
static void criticalError(void *arg){
	//al_bundle_free(&bundle);
}

