# Makefile for compiling DTNfog

# NAME OF BIN USED FOR INSTALL/UNINSTALL (NEVER LEAVE IT BLANK!!!)
BIN_NAME_BASE=dtnfog
CC=gcc
LIB_PATHS=-L/usr/local/lib -L$(UNIFIED_API_DIR)
INSTALL_PATH=/usr/local/bin
DIRS_NEW_AL= -I$(UNIFIED_API_DIR)/src/al/ -I$(UNIFIED_API_DIR)/src/al/socket/ -I$(UNIFIED_API_DIR)/src/al/bundle/ -I$(UNIFIED_API_DIR)/src/al/bundle/options/ -I$(UNIFIED_API_DIR)/src/al/types/ -I$(UNIFIED_API_DIR)/src/al/utilities/ -I$(UNIFIED_API_DIR)/src/al/utilities/debug_print -I$(UNIFIED_API_DIR)/src/al/utilities/bundle_print  
LIBS_BASE=$(UNIFIED_API_DIR)/$(LIB_NAME) -lpthread
DEBUG=0
ifeq ($(DEBUG),0)
DEBUG_FLAG=-O2
else
DEBUG_FLAG=-g -fno-inline -O0
endif
CFLAGS= $(DEBUG_FLAG) -Wall -fmessage-length=0 -Werror 

INSTALLED=$(wildcard $(INSTALL_PATH)/$(BIN_NAME_BASE)*)

ifeq ($(strip $(UNIFIED_API_DIR)),)
all: help
else ifeq ($(or $(strip $(DTN2_DIR)),$(strip $(ION_DIR)),$(strip $(IBRDTN_DIR))),)
# NOTHING
all: help
else
all: bin
endif

BIN_NAME=$(BIN_NAME_BASE)

LIB_NAME=libunified_api


ifneq ($(strip $(DTN2_DIR)),)
LIB_NAME := $(LIB_NAME)_vDTN2
endif

ifneq ($(strip $(ION_DIR)),)
LIB_NAME := $(LIB_NAME)_vION
endif

ifneq ($(strip $(IBRDTN_DIR)),)
LIB_NAME := $(LIB_NAME)_vIBRDTN
endif

ifneq ($(strip $(UD3TN_DIR)),)
LIB_NAME := $(LIB_NAME)_vUD3TN
endif

LIB_NAME := $(LIB_NAME).a

ifneq ($(strip $(DTN2_DIR)),)
# DTN2
INC:= $(INC) -I$(UNIFIED_API_DIR)/src/bp -I$(UNIFIED_API_DIR)/src -I$(DTN2_DIR) -I$(DTN2_DIR)/applib
LIBS:= $(LIBS) $(LIBS_BASE) -ldtnapi
BIN_NAME := $(BIN_NAME)_vDTN2
endif

ifneq ($(strip $(ION_DIR)),)
# ION
INC := $(INC) -I$(UNIFIED_API_DIR)/src/bp -I$(UNIFIED_API_DIR)/src
LIBS := $(LIBS) $(LIBS_BASE) -lbp -lici
BIN_NAME := $(BIN_NAME)_vION
endif

ifneq ($(strip $(IBRDTN_DIR)),)
# IBRDTN
IBRDTN_VERSION=1.0.1
INC := $(INC) -I$(UNIFIED_API_DIR)/src/bp -I$(UNIFIED_API_DIR)/src -I$(IBRDTN_DIR)/ibrcommon-$(IBRDTN_VERSION) -I$(IBRDTN_DIR)/ibrdtn-$(IBRDTN_VERSION)
LIBS := $(LIBS) $(LIBS_BASE) -librcommon -librdtn -lz
BIN_NAME := $(BIN_NAME)_vIBRDTN
endif

ifneq ($(strip $(UD3TN_DIR)),)
# UD3TN
INC := $(INC) -I$(UNIFIED_API_DIR)/src/bp -I$(UNIFIED_API_DIR)/src -I$(UD3TN_DIR) -I$(UD3TN_DIR)/include
LIBS := $(LIBS) $(LIBS_BASE)
BIN_NAME := $(BIN_NAME)_vUD3TN
endif

bin:
	$(CC) $(DIRS_NEW_AL)  $(INC) $(CFLAGS) -c src/*.c
	g++ $(LIB_PATHS) -o $(BIN_NAME) *.o $(LIBS)
#	g++ -o $(BIN_NAME) *.o $(LIBS)

install:
	cp $(BIN_NAME_BASE)* $(INSTALL_PATH)/

uninstall:
	@if test `echo $(INSTALLED) | wc -w` -eq 1 -a -f "$(INSTALLED)"; then rm -rf $(INSTALLED); else echo "MORE THAN 1 FILE, DELETE THEM MANUALLY: $(INSTALLED)"; fi

help:
	@echo "Usage:"
	@echo "For DTN2:                make DTN2_DIR=<dtn2_dir> UNIFIED_API_DIR=<al_bp_dir>"
	@echo "For ION:                 make ION_DIR=<ion_dir> UNIFIED_API_DIR=<al_bp_dir>"
	@echo "For IBRDTN:              make IBRDTN_DIR=<ibrdtn_dir> UNIFIED_API_DIR=<al_bp_dir>"
	@echo "For both DTN2 & ION:     make DTN2_DIR=<dtn2_dir> ION_DIR=<ion_dir> UNIFIED_API_DIR=<al_bp_dir>"
	@echo "For both DTN2 & IBRDTN:  make DTN2_DIR=<dtn2_dir> IBRDTN_DIR=<ibrdtn_dir> UNIFIED_API_DIR=<al_bp_dir>"
	@echo "For both ION & IBRDTN:   make ION_DIR=<ion_dir> IBRDTN_DIR=<ibrdtn_dir> UNIFIED_API_DIR=<al_bp_dir>"
	@echo "For all three:           make DTN2_DIR=<dtn2_dir> ION_DIR=<ion_dir> IBRDTN_DIR=<ibrdtn_dir> UNIFIED_API_DIR=<al_bp_dir>"
	@echo "To compile with debug symbols add DEBUG=1"

clean:
	@rm -rf *.o $(BIN_NAME_BASE)*
